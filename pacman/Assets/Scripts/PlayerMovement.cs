﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 5f;
    float Xaxis;
    float Yaxis;
    bool IsMoving;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {

       
        rb = GetComponent<Rigidbody2D>();

        IsMoving = (Xaxis != 0 || Yaxis != 0);

	}
	
	// Update is called once per frame
	void Update () {
        Xaxis = Input.GetAxisRaw("Horizontal");
        Yaxis = Input.GetAxisRaw("Vertical");
        Debug.Log("xaxis" + Xaxis + "Yaxis" + Yaxis);

        IsMoving = (Xaxis != 0 || Yaxis != 0);

        if (IsMoving)
        {
            Vector2 movevector = new Vector2(Xaxis, Yaxis);
            rb.MovePosition(new Vector2((transform.position.x+movevector.x*speed*Time.deltaTime), (transform.position.y + movevector.y * speed * Time.deltaTime)));

        }
		
	}
}
