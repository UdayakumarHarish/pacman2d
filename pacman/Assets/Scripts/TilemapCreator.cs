﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapCreator : MonoBehaviour {

    public GameObject wall;
    public GameObject floor;
    public GameObject Player;
    public GameObject Colelctable;

    // Start is called before the first frame update
    void Start()
    {
        TextAsset bindata = Resources.Load("newmaze") as TextAsset;
        string stringdata = bindata.text;
        // Debug.Log(stringdata + "str data");
        
        string[] lines = stringdata.Split(new[] { '\n' }, System.StringSplitOptions.None);

        for (int i = 0; i < lines.Length; i++)
        {
            int length = lines[i].ToCharArray().Length;
            for (int j = 0; j < length; j++)
            {
              
                Debug.Log(lines[i].ToCharArray()[j] + "chk");
               

                if (lines[i][j] == 'W')
                {

                    GameObject mWalls=Instantiate(wall, new Vector2(j, i), Quaternion.identity);
                    mWalls.AddComponent<BoxCollider2D>();
                }

                if (lines[i][j] == '2')
                {

                    GameObject mplayer = Instantiate(Player, new Vector2(j, i), Quaternion.identity);
                    mplayer.AddComponent<BoxCollider2D>();

                    GameObject mFLoor = Instantiate(floor, new Vector2(j, i), Quaternion.identity);
                }

                 if (lines[i][j] == '.')
                {
                    GameObject mcollectable = Instantiate(Colelctable, new Vector2(j, i), Quaternion.identity);
                    GameObject mFLoor=  Instantiate(floor, new Vector2(j, i), Quaternion.identity);
                                   

                } 

            }

        }
    }

}
